# Développement web JS avancé

C5-562141-INFO 

Cas du framework ReactJS


![](assets/img/reactjs.png)

Année 2018-2019

---

@transition[fade]

## Organisation de l'UE

+++

### L'équipe enseignante

* Matthieu Robert ( matthieu.robert@univ-lr.fr )
* Clément Dandrieux ( clement.dandrieux@madintec.com )

+++

### Communication

* Questions pédagogiques : [Moodle](https://moodle.univ-lr.fr/)
  * Appronfondissement/Questions (Forum)
  * Organisation de l'UE/Planning (Messages privés)

+++

### Organisation et Evaluation

@ul
* Une partie TD
* Une partie TP évaluée par un enseignant et tests Unitaires
* Attention à la règle sur les absences

@ulend

+++

### Les objectifs de cet enseignement

* Découvrir un framework web de haut niveau (ReactJS)
* Apprendre les bonnes pratiques de développement web (Test Unitaire,test d'intégration)
* Base de nodejs

---

@transition[fade]

# TD / TP 1 Introduction : Les bases


@ul

* Prérequis et ressources
* Objectif du TD/TP
* React, Qu’est-ce que c’est ?

@ulend

+++
## Prérequis

@ul

* les langages HTML et Javascript (les fonctions, les objets, les listes et les classes).
* fonctionnalités d’ES6  (arrow functions, classes, let et const)

@ulend

+++
## Les ressources

@ul
* La bible universelle du Javascript : https://developer.mozilla.org/fr/docs/Web/JavaScript

* La doc officielle (très très bien) de React : https://reactjs.org/docs/getting-started.html


@ulend



---

@transition[fade]

## React, Qu’est-ce que c’est ?

React est une bibliothèque **Javascript déclarative**, efficace et flexible conçu pour faire **des interfaces utilisateur**. 

+++

## Les composants

React permet de créer des interfaces utilisateur complexes depuis de petits morceaux de code isolés appelés “components”.
A vous de tester https://codepen.io/trebormat/pen/maKEWO

```jsx
class Hello extends React.Component {
  render() {
    return (     
      <div>
        <h1>Salut</h1>
      </div>
    )
  }
}
ReactDOM.render(<Hello />, document.getElementById("root"))
```

+++

## La compilation

La syntaxe `<div />` est transformée au moment de la compilation en `React.createElement(‘div’)`. L’exemple précédent est équivalent à ceci :
```jsx
function render()
    {
      return (

        React.createElement("div", null,
          React.createElement("h1", null, "Salut")));


    } }]);return Hello;}(React.Component);

```

+++

## Passer des données avec les “props”

```jsx

class Hello extends React.Component {
  render() {
    return (
     
      <div>
        <h1>Salut {this.props.name}</h1>
      </div>
    )
  }
}

ReactDOM.render(<Hello name="Matt"/>, document.getElementById("root"))
```


+++

## La syntaxe JSX

Avec JSX, vous disposez de toute la puissance du Javascript. Vous pouvez mettre n’importe quelle expression Javascript entre accolades à l’intérieur du JSX. Chaque élément React est un objet Javascript que vous pouvez stocker dans une variable ou utiliser ailleurs dans votre programme.

```jsx
const myElement = <HelloWorld />
```

+++

## Exercice 1 : Créer un composant

Créer un composant `HelloWorld` qui utilisera plusieurs fois le composant `Hello` avec des attributs name différents.

Résultat : https://codepen.io/napkid/full/dwKeYv

+++

## Exercice 2 : Rendre son composant interactif

- Ajouter un bouton dans le composant HelloWorld
- Afficher une alerte dans le navigateur quand ce bouton est cliqué

Résultat : https://codepen.io/napkid/full/dwKeXW

+++

## Exercice 3 : Sauvegarder un état dans son composant

- Ajouter un état initial (initial state) au constructeur du composant HelloWorld
- Faire changer cet état quand on clique sur le bouton
- Afficher le texte "ON" ou "OFF" suivant l'état

Résultat : https://codepen.io/napkid/full/roKvWV

---

@transition[fade]

## Objectifs du TP
Le jeu du morpion ou Tic Tac Toe.
https://playtictactoe.org/

![](assets/img/tictactoe.png)



---

---?include=course/tools/PITCHME.md

