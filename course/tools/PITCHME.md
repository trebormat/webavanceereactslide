# TD / TP 2 : Les outils

## Sommaire

@ul

* NPM
* Webpack
* Babel
* Create-react-app

@ulend

---

## NPM (Node package Manager)

![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/db/Npm-logo.svg/1280px-Npm-logo.svg.png)

+++

@ul

* Installé avec Node.js
* 800.000 paquets open-source
* Outil de gestion des dépendances

@ulend

+++

### Les ressources

@ul

* Le registry officiel : https://www.npmjs.com/
* La liste de toutes les commandes : https://docs.npmjs.com/cli-documentation/cli
* La syntaxe du fichier package.json : https://docs.npmjs.com/files/package.json.html

@ulend

+++

### Le fichier package.json

* Liste les dépendances
* Liste les détails de *votre* package

+++

### Le dossier node_modules

* Contient les paquets installés
* Attention à la taille

+++

### Les commandes

```bash
npm init

npm install

npm remove

npm run
```

+++

---

## La transpilation du Javascript

![](assets/img/js-webpack-babel.png)

+++

### Babel

@ul

* Open source
* Transpilation vous dites ?
* ES6, ES7, React...

@ulend

+++

## Webpack

## Qu'est-ce que c'est ?

* Un module bundler
* Une machine à transformer du code

+++

### Ressources

* Le site officiel : https://webpack.js.org/
* La documentation (pour les curieux) : https://webpack.js.org/concepts/

+++

### Module bundler

![](assets/img/webpack-js-opt.png)

+++

@ul

* Retrace l'arbre des imports
* Fait les liens avec les node_modules
* Assemble les fichiers JS

@ulend

+++

### Transformation

* Babel
* Pré-processeur CSS
* Assets en tous genres
  
---

## Create-react-app

* Configuration difficile de Babel, Webpack et cie
* Un starter template complet et officiel

+++

* Génère un projet vide
* Configuration complète pour React
* Serveur de développement
* Nombreux autres utilitaires
* Communautaire, maintenu par Facebook